import unittest

import simple


class TestSimple(unittest.TestCase):
    """Test check_simple function"""

    def test_one_case_10(self):
        """Docstring"""
        test_number = 10
        result = simple.check_simple(test_number)
        self.assertFalse(result)
